package main

import (
	"fmt"
	"gitlab.com/Burunduck/user-service/internal/app/config"
	ud "gitlab.com/Burunduck/user-service/internal/app/delivery/grpc"
	ur "gitlab.com/Burunduck/user-service/internal/app/repository/psql"
	uu "gitlab.com/Burunduck/user-service/internal/app/usecase"
	"gitlab.com/Burunduck/user-service/internal/models"
	jww "github.com/spf13/jwalterweatherman"
	desc "gitlab.com/Burunduck/user-service/pkg/user_service_api"
	"google.golang.org/grpc"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"net"
	"os"
	"time"
)

func getPostgres() *gorm.DB {
	//TODO: вынести в конфиг
	dsn := os.Getenv("DB_DSN")
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}
	//Только во время разработки автомигрете
	db.AutoMigrate(&models.User{})
	return db
}

func main() {

	cfg, err := config.NewConfig()
	if err != nil {
		jww.ERROR.Println("Config error")
		return
	}

	timeoutContext := 2 * time.Second
	db := getPostgres()
	userRepo := ur.NewUserRepoPsql(db)
	sessionRepo := ur.NewSessionRepo(db)
	userUcase := uu.NewUserUsecase(userRepo, sessionRepo, timeoutContext)

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", cfg.ServiceGrpcPort))
	if err != nil {
		jww.ERROR.Println("Can't listen port")
	}

	serverGrpc := grpc.NewServer()

	desc.RegisterUserServiceServer(serverGrpc, ud.NewServer(userUcase))
	jww.INFO.Printf("Service started at port :%d", cfg.ServiceGrpcPort)
	err = serverGrpc.Serve(lis)
	if err != nil {
		fmt.Println(err)
	}
}
