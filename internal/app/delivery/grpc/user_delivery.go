package endpoint

import (
	"context"
	"fmt"
	"gitlab.com/Burunduck/user-service/internal/models"
	desc "gitlab.com/Burunduck/user-service/pkg/user_service_api"
	"strconv"
)

type Server struct {
	desc.UserServiceServer
	UserUsecase models.UserUsecase
}

func NewServer(userUsecase models.UserUsecase) *Server {
	return &Server{UserUsecase: userUsecase}
}

func (s *Server) Register(ctx context.Context, request *desc.RegisterRequest) (*desc.RegisterResponse, error) {
	u := models.User{
		Login:     request.Login,
		Password:  request.Password,
		FirstName: request.FirstName,
		LastName:  request.LastName,
	}
	//TODO:check on same login
	err := s.UserUsecase.Register(ctx, &u)
	if err != nil {
		return nil, err
	}
	return &desc.RegisterResponse{}, err
}

func (s *Server) Login(ctx context.Context, request *desc.LoginRequest) (*desc.LoginResponse, error) {
	u := models.User{
		Login:    request.Login,
		Password: request.Password,
	}
	session, err := s.UserUsecase.Login(ctx, &u)
	if err != nil {
		return nil, err
	}
	fmt.Println(session)
	return &desc.LoginResponse{
		Session: session,
	}, err
}

func (s *Server) GetUserBySession(ctx context.Context, request *desc.UserBySessionRequest) (*desc.UserBySessionResponse, error) {
	user, err := s.UserUsecase.GetUserBySession(ctx, request.Session)
	fmt.Printf("%+v\n", user)
	if err != nil {
		return &desc.UserBySessionResponse{
			User: nil,
		}, err
	}
	return &desc.UserBySessionResponse{
		User : &desc.User{
			Id:        strconv.Itoa(user.ID),
			FirstName: user.FirstName,
			LastName:  user.LastName,
		},
	}, err
}

func (s *Server) Logout(ctx context.Context, request *desc.LogoutRequest) (*desc.LogoutResponse, error) {
	err := s.UserUsecase.Logout(ctx, request.Session)
	return &desc.LogoutResponse{}, err
}
