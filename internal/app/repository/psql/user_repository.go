package psql

import (
	"context"
	"gitlab.com/Burunduck/user-service/internal/models"
	"gitlab.com/Burunduck/user-service/pkg/utils/hasher"
	"gorm.io/gorm"
)

type UserRepository struct {
	models.UserRepository
	DB *gorm.DB
}

func NewUserRepoPsql(db *gorm.DB) *UserRepository {
	return &UserRepository{DB: db}
}

func (urp *UserRepository) Register(ctx context.Context, u *models.User) error {
	u.Password = hasher.Hash(u.Password)
	err := urp.DB.WithContext(ctx).Create(u).Error
	return err
}

func (urp *UserRepository) GetUserByCreds(ctx context.Context, u *models.User) (int, error) {
	err := urp.DB.WithContext(ctx).First(u, "login = ? AND password = ?", u.Login, hasher.Hash(u.Password)).Error
	if err != nil {
		return -1, err
	}
	return u.ID, nil
}

func (urp *UserRepository) GetUserById(ctx context.Context, id int) (*models.User, error) {
	u := models.User{}
	err := urp.DB.WithContext(ctx).First(&u, "id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return &u, nil
}