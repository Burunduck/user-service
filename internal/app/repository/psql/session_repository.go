package psql

import (
	"context"
	"crypto/sha256"
	"errors"
	"fmt"
	"gitlab.com/Burunduck/user-service/internal/models"
	"gorm.io/gorm"
	"time"
)

type SessionRepository struct {
	DB *gorm.DB
}

func NewSessionRepo(db *gorm.DB) *SessionRepository {
	return &SessionRepository{DB: db}
}

func (sm *SessionRepository) Create(userID int) (string, error) {
	session := genSession(userID)
	sess := models.Session{
		ID:     session,
		UserID: userID,
	}
	err := sm.DB.Create(&sess).Error
	if err != nil {
		return "", err
	}
	return session, nil
}

func genSession(id int) string {
	hash := sha256.Sum256([]byte(fmt.Sprint(id) + fmt.Sprint(time.Now().Unix())))
	session := fmt.Sprintf("%x", hash)
	return session
}

func (sm *SessionRepository) GetUserId(sessionId string) (int, error) {
	sess := models.Session{}
	err := sm.DB.First(&sess, "id = ?", sessionId).Error
	if err != nil {
		return 0, errors.New("No session")
	}
	return sess.UserID, nil
}

func (sm *SessionRepository) DeleteSession(ctx context.Context, session string) error {
	sess := models.Session{}
	err := sm.DB.WithContext(ctx).Where("id = ?", session).Delete(&sess).Error
	if err != nil {
		return err
	}
	return nil
}
